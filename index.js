// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Shaun",
    "lastName": "Smith",
    "email": "shaunsmith@gmail.com",
    "password": "password1234",
    "isAdmin": true,
    "mobileNo": "09431227421"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}
{
    "id": 14,
    "userId": 2,
    "productID" : 26,
    "transactionDate": "11-24-2022",
    "status": "paid",
    "total": 45000
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}
{
    "id": 26,
    "name": "LG Refrigerator",
    "description": "5'2 ft height and two door",
    "price": 45000,
    "stocks": 12,
    "isActive": true,
}